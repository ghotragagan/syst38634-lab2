package test;

import static org.junit.Assert.*;

import org.junit.Test;

import time.Time;

public class TimeTest {
	
	@Test(expected=NumberFormatException.class)
	public void testGetMillisecondsBoundaryOut() {
		int getTotalMilliseconds = Time.getTotalMilliseconds("01:12:11:1000");
		fail("Invalid Amound of Milliseconds");
	}
	 
	@Test
	public void testGetMillisecondsBoundaryIn() {
		int getTotalMilliseconds = Time.getTotalMilliseconds("01:12:11:999");
		assertTrue("Invalid Milliseconds", getTotalMilliseconds == 999);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testGetMillisecondsException() {
		int getTotalMilliseconds = Time.getTotalMilliseconds("01:12:11:1A");
		fail("Invalid Milliseconds");
	}
	
	@Test
	public void testGetMillisecondsRegular() {
		int getTotalMilliseconds = Time.getTotalMilliseconds("01:12:11:10");
		assertTrue("Invalid Milliseconds", getTotalMilliseconds == 10);
	}
	
}


/*
 * 
 * 	@Test(expected=NumberFormatException.class)
	public void testGetMillisecondsBoundaryOut() {
		int getTotalMilliseconds = Time.getTotalMilliseconds("01:12:11:1000");
		fail("Invalid Amound of Milliseconds");
	}
	
	
*/
	
/*
	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("12:00:00");
		assertTrue("The time provided does not match the result", totalSeconds == 43200);
	}
	
	@Test(expected=AssertionError.class)
	public void testGetTotalSecondsBoundaryOut() {
		Time.getTotalSeconds("00:60:00");
		fail("The time provided is not valid!");
	}
	
	@Test(expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		Time.getTotalSeconds("0101:01");
		fail("The time provided is not valid!");
	}
*/