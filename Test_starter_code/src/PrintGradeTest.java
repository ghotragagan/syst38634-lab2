import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PrintGradeTest {

	@Test
	void test() {
		PrintGrade printGrade = new PrintGrade();
		String result = printGrade.print(99);
		assertEquals("You got an A!", result);
	}

}
